/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yanika.shapeframe;

/**
 *
 * @author Acer
 */
public class Circle extends Shape {

    private double raduis;

    public Circle(double raduis) {
        super("Circle");
        this.raduis = raduis;

    }

    public double getRaduis() {
        return raduis;
    }

    public void setRaduis(double raduis) {
        this.raduis = raduis;
    }

    @Override
    public double calArea() {
        return Math.PI*Math.pow(raduis, 2);
    }

    @Override
    public double calPerimeter() {
        return 0.5*Math.PI*raduis;
    }

}
