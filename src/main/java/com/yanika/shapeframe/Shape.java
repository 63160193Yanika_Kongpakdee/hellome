/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yanika.shapeframe;

/**
 *
 * @author Acer
 */
public abstract class Shape {
    private String shapeName ;

    public Shape(String name) {
        this.shapeName = name;
    }

    public String getName() {
        return shapeName;
    }
    public  abstract double calArea();
    public abstract double calPerimeter();
    
}
